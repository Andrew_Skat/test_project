<?php

class report
{

    public function get_report($url){
        require "Config/config.php";
        $db = new PDO($dsn, $username, $password, $options);

        $query = "SELECT *
                    FROM `History`
                    WHERE `Url` = :Url ORDER BY `Created` DESC LIMIT 1";

        $stmt = $db->prepare($query);
        $stmt->execute(array(
                ':Url' => $url,
               )
        );

        $records = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (empty($records)){
            echo "Нет информации по этому домену, выполните сканирование";
        } else {
            echo "Сайт есть в базе! Последнее сканирование: ".$records[0]["Created"]." Имя файла: ".$records[0]["File_Name"];
        }
    }

    public function create_report($url){
        require "Config/config.php";
        $db = new PDO($dsn, $username, $password, $options);

        $site_name = parse_url($url, PHP_URL_HOST);

        $new_report = array(
            "url" => $url,
            "File_Name"  => date("Y-m-d H-i-s-").''.$site_name.'.csv',
        );

        $sql = sprintf(
            "INSERT INTO %s (%s) values (%s)",
            "History",
            implode(", ", array_keys($new_report)),
            ":" . implode(", :", array_keys($new_report))
        );

        $statement = $db->prepare($sql);
        $statement->execute($new_report);
    }
}