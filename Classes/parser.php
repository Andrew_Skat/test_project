<?php
require_once('Classes/report.php');

class parser
{
    public function parse_url($url)
    {
        ini_set('max_execution_time', 600);
        $st_time = microtime(true);


        function curl_get($url){
            $useragent =    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36";
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_REFERER, "https://google.com/");
            ob_start();
            curl_exec($ch);
            print(curl_error($ch));
            curl_close($ch);
            return ob_get_clean();
        }

        function get_content($page){
            preg_match_all('/<img[^>]*?src=["\']?([^"\'\s>]+)["\']?[^>]*?>/is', $page, $images);
            return $images[1];
        }

        function links_array($url){
            $links = [$url];

            $page = curl_get($url);
            preg_match_all('/<a href=\"([^\"]*)\">(.*)<\/a>/iU', $page, $matches);
            $links = array_merge($links, $matches[1]);
            $links = array_unique($links);

            $site_name = parse_url($url, PHP_URL_HOST);
            $fp = fopen(date("Y-m-d H-i-s-").''.$site_name.'.csv', 'w');
            fputcsv($fp, array('URL', 'IMAGES'));

            foreach ($links as $link) {
                if (preg_match("/".$site_name."/", $link, $match)){
                    $page = curl_get($link);
                    $imgs = get_content($page, $link, $fp);
                    foreach ($imgs as $img){
                        fputcsv($fp, array($link, $img));
                    }
                    preg_match_all('/<a.*href="?([^" ]*)" /iU' , $page, $new_links);

                    foreach ($new_links as $new_link){
                        $links = array_merge($links, $new_link);
                        $links = array_unique($links);
                    }
                }
            }
        }

        links_array($url);
        $vars = new report();
        $vars->create_report($url);
        $en_time = microtime(true);
        print 'Время выполнения: '.($en_time-$st_time);
    }
}