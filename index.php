<?php
    require "Config/config.php";
    require_once('Classes/parser.php');
    require_once('Classes/report.php');
    switch ($argv[1]){
        case 'parse':
            $vars = new parser();
            $vars->parse_url($argv[2]);
            break;

        case 'help':
            echo'
                parse - запускает парсер, принимает обязательный параметр url
                report - выводит в консоль результаты анализа для домена, принимает обязательный параметр domain
                help - выводит список команд
            ';
            break;

        case 'report':
            $vars = new report();
            $vars->get_report($argv[2]);
            break;
    }
