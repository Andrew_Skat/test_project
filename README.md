# README #

Test task for Netpeak

# HOW TO USE #

Для использования необходимо:
1)Импортировать базу из дампа dump.sql
2)В папке Config создать и заполнить по примеру файл config.php
3)Будучи в консоли в рабочей директории проекта написать: 
"php index.php help" - для получения помощи
"php index.php parse www.test.com" - для парсинга
"php index.php report www.test.com" - для получения информации о домене

Файл с данными после парсинга сайта создается в корневом каталоге проекта формата "Дата время-адрес_сайта.csv"
### Owner ###

Andrew_Skat